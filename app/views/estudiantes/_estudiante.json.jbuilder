json.extract! estudiante, :id, :idcard, :name, :acaindex, :level, :created_at, :updated_at
json.url estudiante_url(estudiante, format: :json)
