class Api::ChronogramsController < ApplicationController
	skip_before_action :verify_authenticity_token
	before_action :set_estudiantes, only: [:show, :update, :destroy]
	def index
		@chronograms = Chronogram.all
		render json: @chronograms
	end

	def show
	end

	def create
		@chronograms = Chronogram.new(chronograms_params)
		if @chronograms.save
			render json: @chronograms, status: :created
		else
			render json: @chronograms.errors, status: :unprocessable_entity
		end
	end

	def update
		if @chronograms.update(chronograms_params)
			render json: @chronograms, status: :ok
		else
			render json: @chronograms.errors, status: :unprocessable_entity
		end
	end

	def destroy
		@chronograms.destroy
		head :no_content
	end

	public
		def set_estudiantes
			@estudiantes = Estudiante.find(params[:id])
		end

		def chronograms_params
			params.require(:chronogram).permit(:fecha, :hora)
		end
		
end
