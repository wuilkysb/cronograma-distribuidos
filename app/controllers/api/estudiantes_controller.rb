class Api::EstudiantesController < ApplicationController
	skip_before_action :verify_authenticity_token
	before_action :set_estudiantes, only: [:show, :update, :destroy]
	def index
		@estudiantes = Estudiante.all
		render json: @estudiantes
	end

	def show
		chronogram = Chronogram.find(@estudiantes.chronogram_id)
		c = "#{chronogram.fecha} desde las #{chronogram.start_time} hasta las #{chronogram.end_time}"
		render json: @estudiantes.attributes.merge("cronograma": c)
	end

	def create
		@estudiantes = Estudiante.new(estudiantes_params)
		if @estudiantes.save
			render json: @estudiantes, status: :created
		else
			render json: @estudiantes.errors, status: :unprocessable_entity
		end
	end

	def update
		if @estudiantes.update(estudiantes_params)
			render json: @estudiantes, status: :ok
		else
			render json: @estudiantes.errors, status: :unprocessable_entity
		end
	end

	def destroy
		@estudiantes.destroy
		head :no_content
	end

	public
		def set_estudiantes
			@estudiantes = Estudiante.find_by_idcard(params[:id])
		end

		def estudiantes_params
			params.require(:estudiantes).permit(:idcard, :name, :acaindex, :cedula, :apellidos, :level)
		end
		
end
