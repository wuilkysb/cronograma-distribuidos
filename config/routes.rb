Rails.application.routes.draw do
  namespace :api, defaults: {format:'json'} do
    resources :estudiantes, only:[:index, :create,  :destroy, :update, :show]
    resources :chronograms
  end

  resources :estudiantes
end
