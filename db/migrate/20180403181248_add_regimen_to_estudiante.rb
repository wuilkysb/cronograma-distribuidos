class AddRegimenToEstudiante < ActiveRecord::Migration[5.2]
  def change
    add_column :estudiantes, :regimen, :boolean
  end
end
