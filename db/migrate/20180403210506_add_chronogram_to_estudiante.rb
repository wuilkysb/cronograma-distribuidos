class AddChronogramToEstudiante < ActiveRecord::Migration[5.2]
  def change
    add_reference :estudiantes, :chronogram, foreign_key: true
  end
end
