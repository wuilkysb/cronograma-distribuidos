class AddStarttimeToChronogram < ActiveRecord::Migration[5.2]
  def change
    add_column :chronograms, :start_time, :string
    add_column :chronograms, :end_time, :string
  end
end
