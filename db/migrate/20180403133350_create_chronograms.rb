class CreateChronograms < ActiveRecord::Migration[5.2]
  def change
    create_table :chronograms do |t|
      t.date :fecha
      t.time :hora

      t.timestamps
    end
  end
end
