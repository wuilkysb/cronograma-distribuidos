class AddApellidoToEstudiante < ActiveRecord::Migration[5.2]
  def change
    add_column :estudiantes, :apellidos, :string
    add_column :estudiantes, :cedula, :integer
  end
end
