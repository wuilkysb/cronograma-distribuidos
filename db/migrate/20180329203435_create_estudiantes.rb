class CreateEstudiantes < ActiveRecord::Migration[5.2]
  def change
    create_table :estudiantes do |t|
      t.text :idcard
      t.text :name
      t.decimal :acaindex
      t.integer :level

      t.timestamps
    end
  end
end
