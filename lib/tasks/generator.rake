namespace :generator do
  require "faker"
  require 'date'
  desc "TODO"
  task estudiantes: :environment do
    Estudiante.delete_all
    252.times do
      estudiante = Estudiante.create(
        :name => Faker::Name.first_name,
        :idcard => Faker::Number.unique.between(11999999, 35000000),
        :acaindex => Faker::Number.decimal(1,2),
        :level => Faker::Number.number(1),
        :apellidos => Faker::Name.last_name,
        :regimen => Faker::Boolean.boolean
      )
      estudiante.save
    end
  end

  task :cronograma, [:day, :month, :year] => :environment do |t, args|
    Chronogram.delete_all
    init_chronogam = Date.parse("#{args[:year]}-#{args[:month]}-#{args[:day]}").at_beginning_of_week
    end_chronogram = Date.parse("#{args[:year]}-#{args[:month]}-#{args[:day]}").at_end_of_week
    estudiantes = Estudiante.all.order(level: :desc, regimen: :asc, acaindex: :desc).to_a
    estudiantes_cant = 3  
    (init_chronogam...end_chronogram).map do |date| 
      time_init = Time.parse('08:00')
      7.times do
        time_end = time_init + 30.minutes
        
        chronogram = Chronogram.create(
          :fecha => date,
          :start_time => time_init.strftime("%H:%M"),
          :end_time => time_end.strftime("%H:%M")
          )
        chronogram.save
        estudiantes.take(estudiantes_cant).map do |estudiante|
          e = Estudiante.find(estudiante.id)
          e.chronogram_id = chronogram.id
          e.save
        end

        estudiantes.shift(estudiantes_cant)
        time_init = time_end + 5.minutes
      end

      time_init = Time.parse('13:00')
      7.times do
        time_end = time_init + 30.minutes
        
        chronogram = Chronogram.create(
          :fecha => date,
          :start_time => time_init.strftime("%H:%M"),
          :end_time => time_end.strftime("%H:%M")
        )
        chronogram.save
        estudiantes.take(estudiantes_cant).map do |estudiante| 
          e = Estudiante.find(estudiante.id)
          e.chronogram_id = chronogram.id
          e.save
        end
  
        estudiantes.shift(estudiantes_cant)
        time_init = time_end + 5.minutes
      end
    end
  end

  desc "TODO"
  task clean: :environment do
    Estudiante.delete_all
  end

end
